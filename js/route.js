app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $routeProvider.when('/',
  {
    templateUrl: 'view/home_main.html',
    controller: 'homeCtrl'
  })
  .when('/work/',
  {
    templateUrl: baseURL+'view/apphome.html',
    controller: 'appCtrl'
  })

  .when('/ourwork/:jobID',
  {
    templateUrl: baseURL+'view/work.html',
    controller: 'workCtrl'
  })
  .otherwise ({
    templateUrl: baseURL+'view/home.html',
    controller: 'homeCtrl'
  });
  $locationProvider.html5Mode(true);
  // $locationProvider.hashPrefix('#');
}])
.run(function($rootScope, $route, $location){
   $rootScope.$on('$locationChangeSuccess', function() {
        $rootScope.actualLocation = $location.path();
    });        

   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
          $('.view-animate-container').removeClass('back');
        if($rootScope.actualLocation === newLocation) {
             $('.view-animate-container').addClass('back');
        } else {
          // $('.view-animate-container').removeClass('back');
        }
    });
});
