/* ==============

  APP to DEVICE CONTROLLER

================= */

/*
    VARIABLES
*/
var baseURL = window.location.origin+'/';
var currentSlider,newSlider, minSlider ,applianceWatcher,runWatcher, syncObject,sync;

var dbSwitch = (window.location.hostname=='shuntastic.com') ?'https://glowing-torch-1501.firebaseio.com/': (window.location.hostname=='stage.awsmxtp.com')? 'https://stageadv-prototype.firebaseio.com/' : 'https://adv-proto.firebaseio.com/';
// dbSwitch = 'https://stageadv-prototype.firebaseio.com/';

var countdownLED = new Array('P04FFC000','P08FFC000','P12FFC000','P16FFC000','P20FFC000','P24FFC000','P28FFC000','P32FFC000','P36FFC000','P40FFC000','P44FFC000','P48FFC000','P52FFC000','P56FFC000','P60FFC000','P64FFC000','P68FFC000','P72FFC000','P76FFC000','P80FFC000','P85FFC000','P90FFC000','P95FFC000','FFC');



var hndlr = {
  showPrompt: function(newStr) {
    setTimeout(function(){
      $('.lcd #mainprompt.prompt').removeClass('off');
      $('.lcd #mainprompt.prompt').html(newStr);

    },100);

  },
  hidePrompt: function(){

    $('.lcd #mainprompt.prompt').addClass('off');
  },
  showDisabled:function() {
    $('.disabledprompt').addClass('on');
    // $('.lcd #mainprompt').addClass('off');
    setTimeout(function() {
      // $('.lcd #mainprompt').removeClass('off');
      $('.disabledprompt').removeClass('on');
    },2000);
  },
  showRemoteDisablePrompt:function(){
    $('.remotedisabledprompt').addClass('on');
  },
  hideRemoteDisablePrompt:function(){
    $('.remotedisabledprompt').removeClass('on');
  },
  removeAppDisable:function(){
    $('#appdisable').remove();
  },
  showSaveConfirm: function(){
    $('.save').html('FAVORITE SAVED');
    $('.save').addClass('confirmed');
  },
  showApplianceControls: function(){
    $('#appliance-control').addClass('active');
  },
  hideApplianceControls: function(){
    $('#appliance-control').removeClass('active');
  }
}

// ANGULAR
;'use strict';
var app = angular.module('psgeprototype', ['ngRoute', 'ngAnimate','ngResource','firebase']);

// FACTORY
app.factory('Page', function() {
 var title = 'welcome';
 return {
   title: function() { 
      return title;
    },
    setTitle: function(newTitle) { title = newTitle }
  };
})
.controller('mainCon',['$scope', '$routeParams', '$location', '$firebase', '$interval', '$timeout', '$window','Page', function ($scope, $routeParams, $location, $firebase, $interval, $timeout, $window, Page) {

  $scope.mobileCon = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ? ((/IEMobile/i.test(navigator.userAgent)) ? 'mobile iemobile' : 'mobile') : 'desktop';

  $scope.baseURL = baseURL;
  $scope.Page = Page;



  $scope.counter = 100;
  var stopInt,stop,endtime,stopped;
  
  $scope.clockActive = 'on';
  $scope.app_mainstatus = "/view/apphome.html";
  $scope.app_homestatus = "/view/logohome.html";
  $scope.ovenTemps = []

  // $scope.homelcd = "/view/adv_home.html";
  $scope.homelcd = "/view/adv_home_nexus.html";
  $scope.currentlcd = $scope.homelcd;

  $scope.$totalcooktime = '0:00';
  $scope.remainingTime = 0;
  $scope.formTime = formatAppTimer($scope.remainingTime);


  $scope.currentLED = '';
  $scope.setLED = function(newVal,turnoffFlag){
    var setPath = 'https://api.particle.io/v1/devices/290017000347343337373739/color?access_token=4c9a1b83ee3565ea2922c053aa9adc4f039cf4ea';
    if($scope.currentLED !== newVal){
      var oldLED = $scope.currentLED;
      $scope.currentLED = newVal;
      // console.log('LED SENT: '+newVal);
      $.ajax({
        type: "POST",
        url: setPath,
        data:{"arg":newVal},
        success: function(e) {

          if((typeof turnoffFlag !== 'undefined')&&(turnoffFlag)){
            setTimeout(function() {
              $scope.setLED('000');
            },5000);
          }
          // console.log('LED SUCCESS:');
          // console.log(e);
        },
        error: function(e){
          $scope.currentLED = oldLED;
          // console.log('LED ERROR:');
          // console.log(e)
        }
      });
    }

  };
  


  //will pass status info between app and appliance
  $scope.comm = {};
  //INIT FIREBASE
  $scope.initFirebase = function(callback) {
    // console.log('initFirebase',$scope.comm,$scope.comm.length);
    // var fbdb = new Firebase('https://adv-proto.firebaseio.com/');
    if(typeof $scope.comm !== 'undefined') {
      var fbdb = new Firebase(dbSwitch);
      sync = $firebase(fbdb);
      syncObject = sync.$asObject();
      syncObject.$bindTo($scope, "comm").then(function() {
        // console.log('BOUND initFirebase',$scope.comm,$scope.comm.length);
        if(typeof callback !== 'undefined') {
          callback();
        } 
      });

    }

  };
  $scope.initFirebase();


  $scope.resetDB = function(){
    sync.$update({speedcook_title:'',status:'idle',app:'idle',appliance:'idle',timer_running:false,speedcook_ready:false,light:'off',timer_val:0,timer_remaining:'0:00',totalCookTime:'0:00',appliance_trigger:false,app_trigger:false,remote_enabled:false}).then(function(){
          syncObject.presets.speedcook.myrecipes.optionlist = [
          {       name: "Poppers",
                  description: "2 servings, 7 pieces",
                  sizevalue:1.5,
                  slug:"jalpepper",
                  quantvalue:240
          }, {
                  name: "Asparagus",
                  description: "1 piece, medium 4 - 8 oz",
                  sizevalue:2,
                  slug:"roasparagus",
                  quantvalue:240
              }];
              try{
                currentSlider.destroy();
                runWatcher();
                applianceWatcher();
              } catch(e){
                console.log('destroy catch',e);
              }
        syncObject.$save();
      });
  };



  $scope.stopCooking = function() {

    // $timeout.cancel(endtime);
    $interval.cancel(stop);
    sync.$update({timer_running:false,light:'off',speedcook_ready:false,timer_val:0,timer_remaining:'0:00',totalCookTime:'0:00'}).then(function(){
      try{
        // $timeout.cancel(endtime);
        $interval.cancel(stop);
      } catch(e){
        console.log('catch cancel');
      }
      hndlr.hidePrompt();
      $scope.app_homestatus = "/view/logohome.html";
      $location.path('/app');
    });
  };

    $scope.setTimer = function(newTime) {
      sync.$update({timer_val:newTime,timer_remaining:formatAppTimer(newTime),totalCookTime:formatAppTimer(newTime)}).then(function(){
          $totalcooktime = $scope.comm.totalCookTime;
      });
    };
  $scope.startTimer = function(){

      sync.$update({timer_running:true,light:'on'}).then(function(){
        intCount = $scope.remainingTime = $scope.comm.timer_val;
        // endtime = $timeout(function(){$scope.endTimer();},($scope.remainingTime/10)*1000);
        stop = $interval(function() {
          intCount -=10;
          $scope.remainingTime -= 10;
          $scope.formTime = formatAppTimer($scope.remainingTime);
          sync.$update({timer_remaining:formatAppTimer($scope.remainingTime)});

          $scope.setLED(countdownLED[(24-($scope.remainingTime/10))]);

          if ($scope.remainingTime<=0) {$scope.endTimer();};

        },1000,($scope.comm.timer_val/10));
        var runWatcher = $scope.$watch('comm.timer_running', function(newVal,oldVal){
          if(oldVal !== newVal) {
            if($scope.comm.timer_running==false) {
                // $timeout.cancel(endtime);
                $interval.cancel(stop);

            }
          }
        });
      });

  };



   /*   MOBILE APP CONTROL  */
  $scope.remoteWatcher = null;
  $scope.templateStyle = 'disabled';
  $scope.templateStr = 'COOK REMOTELY';
  $scope.templateURL = 'remotecook';


  $scope.appCtrl = function($scope) {

     Page.setTitle('MOBILE APP');
     hndlr.hideRemoteDisablePrompt();

     try{
       $scope.remoteWatcher();
     } catch(e) {
       console.log('remoteWatcher catch',e);
     }


     $scope.initFirebase(function(){
        console.log('called');
       sync.$update({app:'active'}).then(function(){

        console.log('$scope.comm',$scope.comm);
      if ($scope.comm.remote_enabled){
        if($scope.comm.timer_running) {
          $scope.templateStyle = 'cancel';
          $scope.templateStr = 'CANCEL COOKING';
              $scope.templateURL = 'cancel';

        } else {
          $scope.templateStyle = '';
          $scope.templateStr = 'COOK REMOTELY';
              $scope.templateURL = 'remotecook';
        }
      } else {
        if($scope.comm.timer_running) {
          $scope.templateStyle = 'cancel';
          $scope.templateStr = 'CANCEL COOKING';
          $scope.templateURL = 'cancel';

        } else {
          $scope.templateStyle = 'disabled';
          $scope.templateStr = 'COOK REMOTELY';
          $scope.templateURL = 'remotecook';
        }

      }

      var rmc = $scope.$watch('comm.remote_enabled', function(newVal,oldVal){
        console.log('WATCH REMOTE ENABLE',newVal,oldVal);
          if ($scope.comm.remote_enabled){
            if($scope.comm.timer_running) {
              $scope.templateStyle = 'cancel';
              $scope.templateStr = 'CANCEL COOKING';
              $scope.templateURL = 'cancel';

            } else {
              $scope.templateStyle = '';
              $scope.templateStr = 'COOK REMOTELY';
              $scope.templateURL = 'remotecook';
            }
          } else {
            if($scope.comm.timer_running) {
              $scope.templateStyle = 'cancel';
              $scope.templateStr = 'CANCEL COOKING';
              $scope.templateURL = 'cancel';

            } else {
              $scope.templateStyle = 'disabled';
              $scope.templateStr = 'COOK REMOTELY';
              $scope.templateURL = 'remotecook';
            }
          }
          $scope.$apply

       });


        var readyWatcher = $scope.$watch('comm.timer_running',function(newVal,oldVal){
          if($scope.comm.timer_running==true) {
              $scope.app_homestatus = "/view/app_status_timer.html";
          } else {
              $scope.app_homestatus = "/view/logohome.html";
          }
        });
       });
     });
  };
$scope.appCtrlCooking = function($scope) {
  try{
    applianceWatcher();
  } catch(e){
    /**/    
  }
  sync.$update({timer_running:true,light:'on'}).then(function(){
    $scope.app_homestatus = "/view/app_status_timer.html";
    if($scope.comm.remote_enabled == true) {
      // console.log('remoteenabled');
      $scope.templateStyle = 'cancel';
      $scope.templateStr = 'CANCEL COOKING';
      $scope.templateURL = 'cancel';
    } else {
      // console.log('remotedisable');
      $scope.templateStyle = 'disabled';
      $scope.templateStr = 'COOK REMOTELY';
      $scope.templateURL = 'remotecook';

    }
     $scope.apply;

    intCount = $scope.remainingTime = $scope.comm.timer_val;
    // endtime = $timeout(function(){$scope.endTimer();},($scope.remainingTime/10)*1000);
    stop = $interval(function() {
      intCount -=10;
      $scope.remainingTime -= 10;
      $scope.formTime = formatAppTimer($scope.remainingTime);
      syncObject.timer_remaining=formatAppTimer($scope.remainingTime);
      syncObject.$save();

      $scope.setLED(countdownLED[(24-($scope.remainingTime/10))]);

      if ($scope.remainingTime<=0) {$scope.endTimer();};


    },1000,($scope.comm.timer_val/10));

    var rmc = $scope.$watch('comm.remote_enabled', function(newVal,oldVal){
      // console.log('WATCH COOKING URL',newVal,oldVal);
        if ($scope.comm.remote_enabled){
          if($scope.comm.timer_running) {
            $scope.templateStyle = 'cancel';
            $scope.templateStr = 'CANCEL COOKING';
            $scope.templateURL = 'cancel';

          } else {
            $scope.templateStyle = '';
            $scope.templateStr = 'COOK REMOTELY';
            $scope.templateURL = 'remotecook';
          }
        } else {
          if($scope.comm.timer_running) {
            $scope.templateStyle = 'cancel';
            $scope.templateStr = 'CANCEL COOKING';
            $scope.templateURL = 'cancel';

          } else {
            $scope.templateStyle = 'disabled';
            $scope.templateStr = 'COOK REMOTELY';
            $scope.templateURL = 'remotecook';
          }
        }
        $scope.$apply

     });

    var runWatcher = $scope.$watch('comm.timer_running', function(newVal,oldVal){
      if(oldVal !== newVal) {
        if($scope.comm.timer_running==false) {
            // $timeout.cancel(endtime);
            $interval.cancel(stop);
            $location.path('/app');

            // $scope.app_homestatus = "/view/logohome.html";
        }
      }
    });
  });
};


  $scope.appCtrl2 = function($scope) {
     Page.setTitle('MOBILE APP');
     $scope.initFirebase(function(){
       $scope.comm.app = 'active';
       $scope.comm.speedcook_ready = false;
       applianceWatcher = $scope.$watch('comm.timer_running', function(newVal,oldVal){
           if($scope.comm.timer_running===true) {
                 $scope.app_homestatus = "/view/app_status_timer.html";
           } else {
             $scope.app_homestatus = "/view/logohomenorem.html";
           }
       });

     });
  };




    $scope.endTimer = function(){
      // sync.$update({timer_remaining:'COMPLETE'}).then(function(){
      //   $scope.cancelTimer();

      // });
      $scope.stopCooking();
    };
    $scope.cancelTimer = function(){
      $scope.stopCooking();
    };

    $scope.lightToggle = function() {
        if($scope.comm.light=='off') {
          $scope.comm.light='on';
        } else {
          $scope.comm.light='off';
        }
        // $scope.pairCheck();
        $scope.apply
    };


  $scope.homeCtrl = function($scope,$http,Page) {
    $scope.clockActive = 'on';
    $scope.updateScene();
      Page.setTitle('PROTOTYPE | RESET PAGE');
      $scope.initFirebase();
  };

 $scope.showDisabled = function($timeout) {
   hndlr.showDisabled();
 };



/*  APPLIANCE CONTROLS */
var menuFlag = false;
var endFlag = false;

$scope.applianceCtrl = function($scope,Page) {

   hndlr.removeAppDisable();
   Page.setTitle('APPLIANCE');

   $scope.lightToggle = function() {
       if($scope.comm.light=='off') {
         $scope.comm.light='on';
       } else {
         $scope.comm.light='off';
       }
       // $scope.pairCheck();
       $scope.apply;
       // syncObject.$save();
   };
  $scope.currentTimeSet = 0;
  $scope.remainingTime = 0;
  $scope.formTime = formatAppTimer($scope.remainingTime);
  $scope.cur_foodname = '';
  $scope.cur_foodtype = 'chicken';
  $scope.cur_foodselectindex = 1;
  $scope.cur_foodselect = 'boneless';
  $scope.cur_size = 'small';
  $scope.cur_sizedetail = '';
  $scope.cur_sizevalue = 1;
  $scope.cur_quantity = '1-2 pieces';
  $scope.cur_quantityvalue = 240;

  $scope.currentCall = 'cookingmodes';
  $scope.previousCall = $scope.currentCall;
  $scope.currentPowerSet = 0;


  for(var y = 50; y<=140; y++) {
    var z = y*5;
    $scope.ovenTemps.push({value:z,name:z.toString()});
  }
var cookStack = [];

  $scope.remoteEnabledToggle = function() {
      if($scope.comm.remote_enabled==false) {
        $('.remoteprompt').addClass('enabled').removeClass('disabled');
        $scope.comm.remote_enabled=true;
           $scope.setLED('210001520001841001B61001D71001F81');

      } else {
        $scope.comm.remote_enabled=false;
        $scope.setLED('D71001B61001841001520001210001000');
        $('.remoteprompt').addClass('disabled').removeClass('enabled');
      }
      audioHandler.playMode();
      $('.remoteprompt').addClass('on');
      $('.lcd').addClass('off');

      setTimeout(function() {
        if($scope.comm.remote_enabled==true){
          $scope.clearDisplay(false);
          // hndlr.hideApplianceControls();

        }
        $('.remoteprompt').removeClass('on');
        $('.lcd').removeClass('off');
      },2000);

      // $scope.pairCheck();
      $scope.apply
  };


  $scope.refreshPage = function(){
   console.log('refreshPage');   
   try{
      applianceWatcher();
      $interval.cancel(stop);
    } catch(e){
      /**/ 
      console.log('error',e);   
    }
    sync.$update({speedcook_title:'',status:'idle',app:'idle',appliance:'idle',timer_running:false,speedcook_ready:false,light:'off',timer_val:0,timer_remaining:'0:00',totalCookTime:'0:00',appliance_trigger:false,app_trigger:false,remote_enabled:false}).then(function(){
      window.location.reload(false);
      launchIntoFullscreen(document.documentElement);

    });

  };
  $scope.launchFull = function(){
    $scope.setLED('000');
    $('.controlpanel a.launch').css('display','none');

    launchIntoFullscreen(document.documentElement);
    $scope.applianceEnterPressed();
  };




  $scope.clearOffClicked = function() {

    sync.$update({remote_enabled:false}).then(function(){
      $scope.clearDisplay();
    });

  };

  $scope.endSequence = function(){

    function postSequence(){
      $scope.currentlcd = $scope.homelcd;
      $scope.currentCall = 'cookingmodes';
      $('.controlpanel a.launch').css('display','block');
      audioHandler.playEnter();

    }
    $scope.setLED('111001222001333001444001555001666001777001888001ffc010R03');

    var tl = new TimelineMax({repeat:0, yoyo:false, onComplete:postSequence});
    var tracer = $('.roundtimer .tracer');
    var mainFill = $('#rtimer .timefill');
    tracer.removeClass('on');
    tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");
    tl.to(mainFill, 0, {opacity:1,ease: Linear.ease},"+=.5");
    tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");
    tl.to(mainFill, 0, {opacity:1,ease: Linear.ease},"+=.5");
    tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");
    tl.to(mainFill, 0, {opacity:1,ease: Linear.ease},"+=.5");
    tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");
    endFlag = false;

  };

  $scope.startWatch = function() {
    // $scope.initFirebase(function(){
           try{
             applianceWatcher();
           } catch(e){
             /**/    
           }
                 
           sync.$update({appliance:'active'}).then(function(){
              applianceWatcher = $scope.$watchCollection('[comm.speedcook_ready,comm.timer_running]', function(newVal,oldVal){
                if(oldVal[0]!==newVal[0]){

                  if ($scope.comm.speedcook_ready ==true) {

                   $scope.setLED('111001221001332001443001554001665001776001887001ffc');
                   endFlag = true;
                    menuFlag = true;
                    hndlr.showApplianceControls();
                    $scope.currentCall = 'startspeed';
                    $scope.currentlcd = '/view/oven_review_nexus.html';
                  } else {
                    console.log('TRIGGERED',endFlag);

                    if(!endFlag) {
                      menuFlag = false;
                      $scope.currentCall = 'cookingmodes';
                      $scope.currentlcd = $scope.homelcd;
                      $scope.setLED('000');
                      $('.controlpanel a.launch').css('display','block');
                    }


                   // $('.controlpanel .lcd').unbind('click').click(function(){
                   //     $('.controlpanel .lcd').unbind('click');
                   //     launchIntoFullscreen(document.documentElement);
                   //     $scope.applianceEnterPressed();
                   // });


                  } 
                }
                  if ($scope.comm.timer_running == true) {
                
                    menuFlag = false;
                    endFlag = true;
                    // console.log('TIMER RUNNING');
                    $scope.currentlcd = '/view/oven_cooking_nexus.html';
                    hndlr.hidePrompt();
                    $('.enterprompt').removeClass('on');
                    $('.swiper-container').addClass('off');
                  } else if(!menuFlag) {
                    console.log('NOT RUNNING',endFlag);

                    if(endFlag){
                      // console.log('SHOULD RUN');
                      $scope.endSequence();


                    } else {
                      $scope.currentlcd = $scope.homelcd;
                      $scope.currentCall = 'cookingmodes';
                      $('.controlpanel a.launch').css('display','block');
                    }
                    // $('.controlpanel .lcd').unbind('click').click(function(){
                    //     $scope.setLED('000');
                    //     $('.controlpanel .lcd').unbind('click');
                    //     launchIntoFullscreen(document.documentElement);
                    //     $scope.applianceEnterPressed();
                    // });

                    // runWatcher = $scope.$watch('comm.timer_running', function(newVal,oldVal){
                    //     if(oldVal !== newVal){
                    //       if($scope.comm.timer_running == false) {
                    //         $scope.clearDisplay();
                    //         $interval.cancel(stop);
                    //         audioHandler.playMode();
                    //         $scope.startWatch();

                    //       }
                    //     }
                    //  });

                  }
              });
            });

  };


$scope.startWatch();

$scope.clearDisplay = function(audioFlag) {
  // $timeout.cancel(endtime);
  $interval.cancel(stop);
  // clearInterval(stop);

  sync.$update({timer_running:false,timedcook_ready:false,speedcook_ready:false,light:'off',timer_val:0,timer_remaining:'0:00',appliance_trigger:false,totalCookTime:'0:00'}).then(function(){
    try {
      $interval.cancel(stop);
      // clearInterval(stop);
        // $timeout.cancel(endtime);
      } catch(e){
        console.log('cancel catch',e);
      }

     if(!$scope.comm.remote_enabled){ $scope.setLED('000');};

    $scope.previousCall = 'cookingmodes';
    try{
      applianceWatcher();
      runWatcher();
    } catch(e){
      console.log('TRY ERROR',e);
    }
    
    if(typeof audioFlag !== 'undefined') {
        if(audioFlag) audioHandler.playMode();
      } else {
        audioHandler.playMode();
      }


    $scope.currentlcd = $scope.homelcd;
    $scope.currentCall = 'cookingmodes';
    $('.controlpanel a.launch').css('display','block');
    // $('.controlpanel .lcd').unbind('click').click(function(){
    //     $('.controlpanel .lcd').unbind('click');
    //     launchIntoFullscreen(document.documentElement);
    //     $scope.applianceEnterPressed();
    // });

    hndlr.hidePrompt();
    setTimeout(function() {
      // console.log('watch restored');
      $scope.startWatch();
    },2000);

  });
};





$scope.clearOffPressed = function() {
  audioHandler.playMode();
  $scopeclearDisplay();
}

  $scope.setApplianceTimer = function(newTime) {
      applianceWatcher();
      $scope.remainingTime = newTime;
      $scope.comm.timer_val=newTime;
      $scope.comm.timer_remaining=formatAppTimer($scope.remainingTime);
      $scope.formTime = formatAppTimer($scope.remainingTime);
      $scope.comm.totalCookTime=formatAppTimer($scope.remainingTime);

  };
  $scope.endApplianceTimer = function(){
   $interval.cancel(stop);
   // $scope.initFirebase(function(){
    cookStack = [];
    sync.$update({timer_running:false,timedcook_ready:false,speedcook_ready:false,light:'off',timer_val:0,timer_remaining:'0:00',totalCookTime:'0:00'}).then(function(){


      function postSequence(){
        tl.clear();

        $scope.currentlcd = $scope.homelcd;
        $scope.previousCall = 'cookingmodes';
        $scope.currentCall = 'cookingmodes';

        $scope.setLED('111001222001333001444001555001666001777001888001ffc010R03');
        if($scope.comm.remote_enabled) {$scope.setLED('F81');}
        try{
          $interval.cancel(stop);
          applianceWatcher();
          // runWatcher();
        } catch(e){
          console.log('TRY ERROR',e);
        }
        audioHandler.playMode();

        $scope.currentlcd = $scope.homelcd;
        $scope.currentCall = 'cookingmodes';
        $('.controlpanel a.launch').css('display','block');

        hndlr.hidePrompt();
        setTimeout(function() {
          // console.log('watch restored');
          $scope.startWatch();
        },2000);
      }

      var tl = new TimelineMax({repeat:0, yoyo:false, onComplete:postSequence});
      var tracer = $('.roundtimer .tracer');
      var mainFill = $('#rtimer .timefill');
      tracer.removeClass('on');
      tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");
      tl.to(mainFill, 0, {opacity:1,ease: Linear.ease},"+=.5");
      tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");
      tl.to(mainFill, 0, {opacity:1,ease: Linear.ease},"+=.5");
      tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");
      tl.to(mainFill, 0, {opacity:1,ease: Linear.ease},"+=.5");
      tl.to(mainFill, 0, {opacity:0,ease: Linear.ease},"+=.5");

    });
   // });
    // $scope.resetAppliance();
  };


   $scope.startOven = function() {
    // applianceWatcher();
    $scope.currentlcd = '/view/oven_temp.html';
    audioHandler.playMode();
   };


   $scope.scrollUp = function() {
    // console.log('$scope.currentSlider',currentSlider);
      currentSlider.swipeNext();
      audioHandler.playArrow();
   };
   $scope.scrollDown = function() {
      currentSlider.swipePrev();
      audioHandler.playArrow();
   };
   $scope.showCookingModes = function(){
    menuFlag = true;
      applianceWatcher();
      // hndlr.showPrompt('Mode<br />Select');
      $scope.currentlcd = '/view/oven_cookingmode_main_nexus.html';
   }
   $scope.showCookingOptions = $scope.startMicrowave = function() {
     menuFlag = true;

      applianceWatcher();
      hndlr.showPrompt('Option<br />Select');
       $scope.currentlcd = '/view/oven_cookingmode_menu.html';
     // audioHandler.playMode();
   };
    $scope.startApplianceSpeedCook = function() {
      // applianceWatcher();
      menuFlag = true;
      hndlr.showPrompt('Food Type Select');
      $scope.currentlcd = '/view/oven_speedcook_start_nexus.html';
      // audioHandler.playMode();
    };

    var prevVal ='';
  $scope.updateCall = function(e) {
    var x = e.container;
    var y = $(x).find('.swiper-slide-active').data('val');
    y = (y !==undefined)?y:prevVal;
    // console.log('updateCall',y);
    $scope.currentCall = prevVal = y;
    // console.log('changed',$scope.currentCall);
  };

  $scope.currentMin =0;
  $scope.currentSec =0;
  $scope.minutesSet = false;

  $scope.updateCallandTimer = function(e) {
    var x = e.container;
    var y = $(x).find('.swiper-slide-active').data('val');
    var z = $(x).find('.swiper-slide-active').data('minutes');
    var zz = $(x).find('.swiper-slide-active').data('seconds');
    var zzz = $(x).find('.swiper-slide-active').data('power');
    if(z){
      $scope.currentMin = z*60;
 //     $scope.currentTimeSet = z*60;
    }
    if(zz){
      $scope.currentSec = zz;
      // $scope.currentTimeSet += zz;
    }
    if(zzz){
      $scope.currentPowerSet = zzz;
    }
    $scope.currentCall = y;
    // console.log('currentcall', $scope.currentCall,'min',z,'sec',zz,'power',zzz);
  };
  // $scope.appliancePrompt = '<div>Power Lvl<br />Select</div>';
  $scope.applianceBackPressed = function(){
    sync.$update({remote_enabled:false});
    if($scope.comm.timer_running===false){

      if($scope.previousCall==''){
        $scope.currentlcd = $scope.homelcd;
        $scope.currentCall = 'cookingmodes';
        hndlr.hidePrompt();
      } else {
        $scope.currentCall = $scope.previousCall;
        $scope.applianceEnterPressed();
      }

    } else {

      audioHandler.playEnter();

    }

  }
  $scope.applianceSpeedEditPressed = function(){
      $scope.currentCall = 'speedcook';
      $scope.applianceEnterPressed();
  };


$scope.applianceStartPressed = function(){
  sync.$update({remote_enabled:false});
  switch($scope.currentCall) {
    case 'startspeed':
    
    sync.$update({appliance_trigger:true}).then(function(){
      $scope.currentlcd = '/view/oven_cooking_nexus.html';
      $scope.startApplianceTimer();
      $scope.currentCall = '';
      // $('.enterprompt').removeClass('on');
      // $('.swiper-container').addClass('off');
    });

    break;

    case 'starttimed':

    sync.$update({appliance_trigger:true,speedcook_title:'Micro Timed Cook',timedcook_ready:true}).then(function(){
      $scope.currentlcd = '/view/oven_cooking_nexus.html';
      $scope.startTimedTimer();
      $scope.currentCall = '';
    });

    break;


    case 'pausetimer':
    $scope.showApplianceDisabled();
    break;

    default:
      $scope.showApplianceDisabled();
    break;
  }
  audioHandler.playEnter();


}
 $scope.applianceEnterPressed = function(newCall){
  // cookStack.push($scope.currentCall);
  if(typeof newCall !== 'undefined'){
    $scope.currentCall = newCall;
  }

  sync.$update({remote_enabled:false});
  switch($scope.currentCall) {

    case 'startspeed':
    case 'starttimed':
      /**/
    break;

    case 'cookingmodes':
        $scope.previousCall = '';

        $scope.currentCall = '';
        $scope.showCookingModes();
        audioHandler.playEnter();
    break;

    case 'microwave':
     $scope.previousCall = 'cookingmodes';
     $scope.startMicrowave();
      audioHandler.playEnter();
    break;

    case 'speedcook':
      $scope.previousCall = 'cookingmodes';
      $scope.startApplianceSpeedCook();
      audioHandler.playEnter();
    break;

    case 'resumetimer':
    $scope.toggleTimer();
    audioHandler.playEnter();

    break;

    case 'pausetimer':
    $scope.showApplianceDisabled();
    audioHandler.playEnter();
    break;

    case 'edit':
    $scope.previousCall = 'cookingmodes';
      hndlr.showPrompt('Food<br />Type<br />Select');
      $scope.currentlcd = '/view/oven_speedcook_start_nexus.html';
      audioHandler.playEnter();
    break;

    case 'timed':
      $scope.previousCall = 'cookingmodes';

       $scope.currentMin =0;
       $scope.currentSec =0;
       $scope.currentTimeSet = 0;

        hndlr.showPrompt('Cook<br />Time');
        $('.ovenpanel .minutes, .prompt .minutes').each(function(){$(this).addClass('active')});
        $('.ovenpanel .seconds, .prompt .second').each(function(){$(this).removeClass('active')});
        currentSlider = minSlider;

        $scope.currentlcd = '/view/oven_timedcook.html';
        audioHandler.playEnter();
   break;

    case 'myrecipes':
      $scope.previousCall = 'cookingmodes';

      $scope.cur_foodtype = 'myrecipes';
      hndlr.showPrompt('Food Select');
      $scope.currentlcd = '/view/oven_speedcook_nexus_1.html';
      audioHandler.playEnter();
    break;

    case 'chicken':
    $scope.previousCall = 'edit';

      $scope.cur_foodtype = 'chicken';
      hndlr.showPrompt('Food Select');
      $scope.currentlcd = '/view/oven_speedcook_nexus_1.html';
      audioHandler.playEnter();
    break;

    case 'boneless':
    case 1:
    case $scope.cur_foodselectindex:
    case $scope.cur_foodselect:
    $scope.previousCall = 'chicken';

      hndlr.showPrompt('Size Select');
      $scope.currentlcd = '/view/oven_speedcook_nexus_2.html';
      audioHandler.playEnter();
    break;

    case 'small':
    case $scope.cur_size:
    $scope.previousCall = 'boneless';
      // $scope.currentCall = $scope.cur_quantity;
      hndlr.showPrompt('Size Select');
      $scope.currentlcd = '/view/oven_speedcook_nexus_3.html';
      audioHandler.playEnter();
    break;

    case '1-2 pieces':
    case $scope.cur_quantity:
    $scope.previousCall = 'small';

      hndlr.hidePrompt();
      currentSlider.destroy();
     var newTime = $scope.cur_sizevalue*$scope.cur_quantityvalue;
    //  console.log('newTime',$scope.cur_sizevalue,$scope.cur_quantityvalue);
      $scope.setApplianceTimer(newTime);

      //COOKING REVIEW LIGHT - WHITE
      $scope.setLED('111001221001332001443001554001665001776001887001ffc');

      $scope.currentCall = 'startspeed';
      $scope.currentlcd = '/view/oven_review_nexus.html';

      audioHandler.playEnter();
    break;

    case 'timedminutes':
//      $scope.minutesSet = true;
      $scope.previousCall = 'timed';

      $scope.currentCall = 'timedseconds';

      $('.ovenpanel .minutes, .prompt .minutes').each(function(){$(this).removeClass('active')});
      $('.ovenpanel .seconds, .prompt .second').each(function(){$(this).addClass('active')});
      currentSlider = newSlider;
      audioHandler.playEnter();
    break;

    case 'timedseconds':
    $scope.previousCall = 'timed';

      // console.log('$scope.currentMin + $scope.currentSec',$scope.currentMin,$scope.currentSec)
      if(($scope.currentMin + $scope.currentSec) > 0){
        $scope.setApplianceTimer(($scope.currentMin + $scope.currentSec));
        $scope.currentlcd = '/view/oven_timedcook_power.html';
        hndlr.showPrompt('Power<br />Level<br />Select');
        audioHandler.playEnter();
        // $scope.currentTimeSet = 0;
      }
    break;

    case 'timedpower':
      $scope.previousCall = 'timedseconds';

      if($scope.currentPowerSet > 0){
        currentSlider.destroy();
        hndlr.showPrompt('');
        hndlr.hidePrompt();
        $scope.currentCall = 'starttimed';
       $scope.currentlcd = '/view/oven_review_timedcook.html';
        audioHandler.playEnter();
        // $scope.startPausePressed = function(){$scope.applianceEnterPressed();};
      }
    break;

    default:
      $scope.showApplianceDisabled();
    break;
  }

 };


// $scope.resetAppliance();



  // $scope.startPausePressed = 

  $scope.initialPausedPressed = function() {
     $scope.initFirebase(function(){

     if($scope.comm.timer_running===true) {
       $scope.currentCall = 'pausetimer';
       $scope.applianceEnterPressed();
     } else {
       $scope.currentCall = 'resumetimer';
       $scope.applianceEnterPressed();
     }
    });
  };



   $scope.showApplianceDisabled = function($timeout) {
         $('.disabledprompt').addClass('on');
         // $('.controlpanel .view-animate-container').addClass('off');
         

         $('.lcd').addClass('off');

         setTimeout(function() {
           $('.disabledprompt').removeClass('on');
           // $('.controlpanel .view-animate-container').removeClass('off');
         $('.lcd').removeClass('off');
         },2000);
   };


  $scope.startTimedTimer = $scope.startApplianceTimer = function(){
     try{
      applianceWatcher();
    } catch(e) {
      console.log('applianceWatch Error',e);
    }
     hndlr.hidePrompt();
     // cookStack = [];
     menuFlag = false;

     // $scope.initFirebase(function(){
      if($scope.comm.timer_val > 0){
        sync.$update({light:'on',timer_running:true}).then(function(){
          $interval.cancel(stop);
          // clearInterval(stop);
          // $timeout.cancel(endtime);

           intCount = $scope.remainingTime = $scope.comm.timer_val;
           // endtime = $timeout(function(){$scope.endApplianceTimer();},($scope.remainingTime/10)*1000);

           $scope.setLED(countdownLED[0]);

           stop = $interval(function() {
           // stop = window.setInterval(function() {
              intCount -=10;
              $scope.remainingTime -= 10;
              $scope.formTime = formatAppTimer($scope.remainingTime);
              sync.$update({timer_remaining:formatAppTimer($scope.remainingTime)});

              $scope.setLED(countdownLED[(24-($scope.remainingTime/10))]);

              if ($scope.remainingTime<=0) {$scope.endApplianceTimer();};

            },1000,($scope.comm.timer_val/10));

              // runWatcher = $scope.$watch('comm.timer_running', function(newVal,oldVal){
              //     if(oldVal !== newVal){
              //       if($scope.comm.timer_running == false) {
              //         $scope.clearDisplay();
              //         $interval.cancel(stop);
              //         audioHandler.playMode();
              //         $scope.startWatch();

              //       }
              //     }
              //  });

          });
      }

  };

  $scope.updateStatus = function(newEl){
    $(newEl).css('left','166px').css('font-size','30px').css('opacity','0').prependTo('.reviewstatus').animate({left:'0px',fontSize:'24px',opacity:'1'},300);
  };


  var xflag = false;


};


  $scope.toggleTimer = function(){
//    $scope.comm.light = 'off';
    if($scope.comm.timer_running) {
      $interval.cancel(stop);
      // $timeout.cancel(endtime);
      sync.$update({timer_running:false});

    } else {
      var intCount = $scope.remainingTime;
      sync.$update({timer_remaining:formatAppTimer(intCount),timer_val:intCount}).then(function(){
        $scope.startTimedTimer();

      });
    }
  };

$scope.remoteCookWatch = function() {
 $scope.remoteWatcher = $scope.$watch('comm.remote_enabled', function(newVal,oldVal){
    if(oldVal !== newVal){
      // console.log('watching',newVal,oldVal);
      if ((!$scope.comm.remote_enabled)&&(window.location.pathname!=='/app/')){
      // if ((!$scope.comm.remote_enabled)&&($location.path()!==='/app/')){
        hndlr.showRemoteDisablePrompt();
        $scope.apply
      }
    }
  });


};



  $scope.browseBack = function(){
    $window.history.back();
    // history.back();
    // $scope.$apply();
  };


  $scope.remotecook = function(){
    Page.setTitle('COOKING MODE');
        $scope.remoteCookWatch();
        sync.$update({speedcook_ready:false});
  };
   $scope.myrecipesstart = function(){
      Page.setTitle('FAVORITES');
     //  $scope.comm.speedcook_ready = false;
  };





   $scope.startSpeedCook = function() {
      $scope.remoteCookWatch();
      sync.$update({speedcook_ready:false,timer_remaining:'0:00',totalCookTime:'0:00'});
      Page.setTitle('FOOD TYPE');
   };
   $scope.showFoodOptions = function($routeParams) {
    if($routeParams.fdID == 'chicken') {
      // $scope.remoteCookWatch();
      $scope.fdID = $routeParams.fdID;
      $scope.initFirebase(function() {
        Page.setTitle($scope.comm.presets.speedcook[$scope.fdID].name);
        $scope.foodOptions = $scope.comm.presets.speedcook[$scope.fdID].optionlist;

      });
    } else {
      $scope.showDisabled();
      $scope.browseBack();
    }
  };

   $scope.showOptions = function($routeParams) {
    if($routeParams.optID == 1) {
      try{
        $scope.remoteWatcher();
      } catch(e) {
        console.log('e',e);
      }
      // $scope.remoteCookWatch();
      $scope.fdID = $routeParams.fdID;
      $scope.optID = $routeParams.optID;
      $scope.initFirebase(function() {
        var x = $scope.comm.presets.speedcook[$scope.fdID].optionlist;
        var y = $scope.comm.presets.speedcook[$scope.fdID].name;
        $scope.z = x[$scope.optID].slug;
        var cookdetailname = x[$scope.optID].name + ' ' + y;
        Page.setTitle(cookdetailname);
        $scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;
        $scope.sizes = $scope.comm.presets.speedcook[$scope.fdID].size;
        $scope.current_size = $scope.sizes[0].label;
        $scope.current_sizedetail = $scope.sizes[0].detail;
        $scope.current_sizeval = $scope.sizes[0].value;

        $scope.current_quantity = $scope.quantities[0].label;
        $scope.current_quantVal = $scope.quantities[0].value;
        
        var newTime = $scope.current_sizeval*$scope.current_quantVal;

        $scope.app_homestatus = "/view/app_status_timer.html";
        $scope.remainingTime = newTime;
        // $scope.comm.timer_remaining = formatAppTimer(newTime);
        $totalcooktime =  formatAppTimer(newTime);


        sync.$update({speedcook_title:cookdetailname,speedcook_ready:true,timer_val:newTime,timer_remaining:formatAppTimer(newTime),totalCookTime:formatAppTimer(newTime)}).then(function(){
          // applianceWatcher();
          applianceWatcher = $scope.$watch('comm.timer_running', function(newVal,oldVal){
            // console.log('showOptions watcher timer');

            if(oldVal !== newVal) {
              if($scope.comm.timer_running==true) {
                    applianceWatcher();
                    $location.path('/app/');
              }
            }
          });

        });

        $timeout(function() {

          $('.panel.sizes .item:first-child a').addClass('current');
          $('.panel.quantities .item:first-child a').addClass('current');
          $('.scoptions').accordion({
            header:'div.tab',
            heightStyleType:'content',
            collapsible: true,
            active : false,
            beforeActivate: function( event, ui ) {

            }
          });

        },5);

     });
    } else {
      $scope.showDisabled();
      $scope.browseBack();
    }
    $scope.setCurrentSize = function(newSize,newDetail,newVal,event){
      $('.sizes .item a').each(function(){
        $(this).removeClass('current');
      });
      $(event.currentTarget).addClass('current');
      $('.scoptions').accordion({active : false,collapsible: true});
        $scope.current_size = newSize;
        $scope.current_sizedetail = newDetail;
        $scope.current_sizeval = newVal;
        $scope.setTimer($scope.current_sizeval*$scope.current_quantVal);

    };
    $scope.setCurrentQuant = function(newQuant,newVal,event){
      $('.quantities .item a').each(function(){
        $(this).removeClass('current');
      });
      $(event.currentTarget).addClass('current');
      $('.scoptions').accordion({active : false,collapsible: true});
        $scope.current_quantity = newQuant;
        $scope.current_quantVal = newVal;
        $scope.setTimer($scope.current_sizeval*$scope.current_quantVal);

    };
  };

  $scope.addRecipePrompt = function() {
    $('.addrecipeprompt').addClass('on');

  };
   $scope.showMRmenu = function() {
     $scope.initFirebase(function() {
       $scope.foodOptions = $scope.comm.presets.speedcook.myrecipes.optionlist;
     });
     Page.setTitle('FAVORITES');
   };

     $scope.showMRMode = function() {
        $scope.initFirebase();
        Page.setTitle('COOKING MODE');
        // $scope.comm.speedcook_ready = false;

     };
     $scope.showMyRecipesFoodType = function() {
        $scope.initFirebase();
        Page.setTitle('FOOD TYPE');
        // $scope.comm.speedcook_ready = false;

     };



    $scope.showMRFoodOptions = function($routeParams) {
     if($routeParams.fdID == 'potatoes') {
       $scope.fdID = $routeParams.fdID;
       $scope.initFirebase(function() {
         Page.setTitle($scope.comm.presets.speedcook[$scope.fdID].name);
         $scope.foodOptions = $scope.comm.presets.speedcook[$scope.fdID].optionlist;

       });
     } else {
       $scope.showDisabled();
       $scope.browseBack();
     }
   };
    $scope.showMROptions = function($routeParams) {
     if($routeParams.optID == 0) {
       $scope.fdID = $routeParams.fdID;
       $scope.optID = $routeParams.optID;
       $scope.initFirebase(function() {
         var x = $scope.comm.presets.speedcook[$scope.fdID].optionlist;
         var y = $scope.comm.presets.speedcook[$scope.fdID].name;
         $scope.z = x[$scope.optID].slug;
         var cookdetailname = x[$scope.optID].name;
         Page.setTitle(cookdetailname);
         $scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;
         $scope.sizes = $scope.comm.presets.speedcook[$scope.fdID].size;
         $scope.current_size = $scope.sizes[0].label;
         $scope.current_sizedetail = $scope.sizes[0].detail;
         $scope.current_sizeval = $scope.sizes[0].value;

         $scope.current_quantity = $scope.quantities[0].label;
         $scope.current_quantVal = $scope.quantities[0].value;

         // $scope.comm.speedcook_title = 'Baked Potato';
         // $scope.comm.speedcook_ready = true;
         // $scope.comm.timer_remaining = formatAppTimer($scope.remainingTime);

         $scope.setTimer($scope.current_sizeval*$scope.current_quantVal);
         sync.$update({speedcook_title:'Baked Potato',timer_val:$scope.remainingTime,timer_remaining:formatAppTimer($scope.remainingTime)}).then(function(){
          $scope.formTime = formatAppTimer($scope.remainingTime);


         });


         $timeout(function() {
          $('.panel.sizes .item:first-child a').addClass('current');
          $('.panel.quantities .item:first-child a').addClass('current');
           $('.scoptions').accordion({
             header:'div.tab',
             heightStyleType:'content',
             collapsible: true,
             active : 'none',
             beforeActivate: function( event, ui ) {

             }
           });
         },100);

       });
     } else {
       $scope.showDisabled();
       $scope.browseBack();
     }

       $scope.setCurrentSize = function(newSize,newDetail,newVal,event){
         $('.sizes .item a').each(function(){
           $(this).removeClass('current');
         });
         $(event.currentTarget).addClass('current');
         $('.scoptions').accordion({active : false,collapsible: true});
           $scope.current_size = newSize;
           $scope.current_sizedetail = newDetail;
           $scope.current_sizeval = newVal;
           $scope.setTimer($scope.current_sizeval*$scope.current_quantVal);

       };
       $scope.setCurrentQuant = function(newQuant,newVal,event){
         $('.quantities .item a').each(function(){
           $(this).removeClass('current');
         });
         $(event.currentTarget).addClass('current');
         $('.scoptions').accordion({active : false,collapsible: true});
           $scope.current_quantity = newQuant;
           $scope.current_quantVal = newVal;
           $scope.setTimer($scope.current_sizeval*$scope.current_quantVal);

       };

    
     $scope.saveToRecipes = function(){
      hndlr.showSaveConfirm();
      $timeout(function(){
        $location.path('/app');
        $location.path('/app/myrecipes');
      },2000);
      var newDesc = $scope.current_quantity + ', '+$scope.current_size+' ('+$scope.current_sizedetail+')';
       var newRec = {
         name: 'Baked Potato',
         quantvalue: $scope.current_quantVal,
         sizevalue: $scope.current_sizeval,
         description: newDesc,
         slug:$scope.z
       }
       $scope.comm.presets.speedcook.myrecipes.optionlist.push(newRec);
     };
   };

  $scope.updateScene = function(){
    $("#main-nav").removeClass("on");
  };

}])
.directive('clock',function($location,$interval){
  function link(scope, element,attrs) {
          //DISPLAYS CLOCK
          var d = new Date();
          scope.clock = returnCurrentTime(d);
          $interval(function() {
            // console.log('interval');
            var nd = new Date();
            scope.clock = returnCurrentTime(nd);
          },1000);

      }
      return {
        restrict:'E',
        transclude:true,
        link: link,
        controller:function(){


        },
replace: true,
template: '<div class="clock {{clockActive}}">{{clock[0]}}</div>'
};
})
.directive('remoteindicatornexus',function($location,$interval){
  function link(scope, element,attrs) {

    scope.activeFlag = (scope.comm.remote_enabled) ? 'on' : '';
    scope.bnlabel = (scope.comm.remote_enabled) ? 'REMOTE ENABLED' : 'ENABLE REMOTE';
    scope.$watch('comm.remote_enabled', function(newVal,oldVal){
      if(oldVal !== newVal){
        scope.activeFlag = (scope.comm.remote_enabled) ? 'on' : '';
        scope.bnlabel = (scope.comm.remote_enabled) ? 'REMOTE ENABLED' : 'ENABLE REMOTE';
      }
    });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller:function(){


    },
    replace: true,
    template: '<div ng-click="remoteEnabledToggle()" class="remote {{activeFlag}}" ng-bind="bnlabel">{{bnlabel}}</div>'
  };
})
.directive('remoteindicator',function($location,$interval){
  function link(scope, element,attrs) {

        scope.activeFlag = (scope.comm.remote_enabled) ? 'on' : '';
        scope.$watch('comm.remote_enabled', function(newVal,oldVal){
          if(oldVal !== newVal){
              scope.activeFlag = (scope.comm.remote_enabled) ? 'on' : '';
            }
         });

      }
      return {
        restrict:'E',
        transclude:true,
        link: link,
        controller:function(){


        },
replace: true,
template: '<div class="remoteenabled {{activeFlag}}"></div>'
};
})
.directive('appremoteindicator',function($location,$interval){
  function link(scope, element,attrs) {

        scope.activeFlag = (scope.comm.remote_enabled) ? 'on' : 'off';
        scope.$watch('comm.remote_enabled', function(newVal,oldVal){
          if(oldVal !== newVal){
            scope.activeFlag = (scope.comm.remote_enabled) ? 'on' : 'off';
          }
         });

      }
      return {
        restrict:'E',
        transclude:true,
        link: link,
        controller:function(){


        },
replace: true,
template: '<div class="remoteenabled {{activeFlag}}"></div>'
};
})
.directive('appremotecookingbn',function($location,$interval){
  function link(scope, element,attrs) {

    scope.templateStr = '';
    scope.templateStyle = '';
    scope.templateURL = '';

    if (scope.comm.remote_enabled){
      if(scope.comm.timer_running) {
        scope.templateStyle = 'cancel';
        scope.templateStr = 'CANCEL COOKING';
            scope.templateURL = 'cancel';

      } else {
        scope.templateStyle = '';
        scope.templateStr = 'COOK REMOTELY';
            scope.templateURL = 'remotecook';
      }
    } else {
      if(scope.comm.timer_running) {
        scope.templateStyle = 'cancel';
        scope.templateStr = 'CANCEL COOKING';
        scope.templateURL = 'cancel';

      } else {
        scope.templateStyle = 'disabled';
        scope.templateStr = 'COOK REMOTELY';
        scope.templateURL = 'remotecook';
      }

    }

scope.$watchCollection('[comm.remote_enabled,templateStyle,templateStr,templateURL]', function(newVal,oldVal){
  // console.log('WATCH COOKING URL',newVal,oldVal);
    if (scope.comm.remote_enabled){
      if(scope.comm.timer_running) {
        scope.templateStyle = 'cancel';
        scope.templateStr = 'CANCEL COOKING';
        scope.templateURL = 'cancel';

      } else {
        scope.templateStyle = '';
        scope.templateStr = 'COOK REMOTELY';
            scope.templateURL = 'remotecook';
      }
    } else {
      if(scope.comm.timer_running) {
        scope.templateStyle = 'cancel';
        scope.templateStr = 'CANCEL COOKING';
        scope.templateURL = 'cancel';

      } else {
        scope.templateStyle = 'disabled';
        scope.templateStr = 'COOK REMOTELY';
        scope.templateURL = 'remotecook';
      }
    }
     scope.e = newVal[1];
      scope.templateStr = newVal[2];
      scope.templateURL = newVal[3];
      scope.apply;

 });


      }
      return {
        restrict:'E',
        transclude:true,
        link: link,
        controller:function(){


        },
        replace: true,
        template: '<div><a class="{{templateStyle}}" href="{{baseURL}}#/app/{{templateURL}}/">{{templateStr}}</a></div>'
        // template: '<div><a class="{{templateStyle}}" href="{{baseURL}}#/app/{{templateURL}}/" ng-bind="templateStr">{{templateStr}}</a></div>'
    };
})
.directive('controlbuttons',function($location,$interval){
  function link(scope, element,attrs) {
      //
      }
      return {
        restrict:'E',
        transclude:true,
        link: link,
        controller:function(){


        },
replace: true,
templateUrl: 'view/buttons.html'
};
})
.directive('backbn', function(){
    return {
      restrict: 'A',

      link: function(scope, element, attrs) {
        element.bind('click', scope.browseBack);
      }
    }
})
.directive('disablebn', function(){
    return {
      restrict: 'A',

      link: function(scope, element, attrs) {
        element.bind('click', scope.showDisabled);
      }
    }
})
.directive('appheader', function(){
    return {
      restrict: 'E',

      link: function(scope, element, attrs) {
        element.bind('click', goBack);
        function goBack() {
          menuFlag = false;
            sync.$update({speedcook_ready:false});
        }
      },
    replace: true,
    template:'<div class="apphdr"><a class="backBn" href backbn></a><p ng-bind="Page.title()">COOKING MODE</p><a href="{{baseURL}}#/app/" class="closeBn"></a></div>'

    }
})
.directive('ovenslider',function($timeout){
  function link(scope,element,attrs){
   $timeout(function() {
      currentSlider = $('.swiper-container').swiper({ 
        speed:100, 
        slideshow:false,
        controlNav: false,
        directionNav: false,
        slidesPerViewFit:true,
        visibilityFullFit:true,
        cssWidthAndHeight:true,
        slidesPerView:'auto',
        centeredSlides:true,
        loop:false,
       mode:'vertical',
       onInit:function() {
          //
            
        }
      });
  },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in '+attr.type+'" data-val="'+attr.type+'"><p>{{option.name}}</p></div></div></div>';
    }
  }

})
.directive('colorpick',function($timeout){
  function link(scope,element,attrs){
   $timeout(function() {
        $('#cp').spectrum({
          color: scope.comm.lcdcolor,
          showInput: true,
          change: function(color) {
              var t = $("#cp").spectrum("get");
              sync.$update({lcdcolor:t.toHexString()}).then(function(){
                $('.cpholder').removeClass('on');
              });
              
          }
      });
  },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="cpholder"><input type="text" id="cp" /></div>';
    }
  }

})
.directive('ovenmenuslider',function($timeout){
  function link(scope,element,attrs){
    scope.menuOptions = [
      {value:'edit',name:'edit'},
      {value:'startspeed',name:'start'},
      {value:'save',name:'save'}
    ];
   $timeout(function() {
      currentSlider = $('.swiper-container').swiper({ 
        speed:100, 
        slideshow:false,
        controlNav: false,
        directionNav: false,
        initialSlide:1,
        slidesPerViewFit:true,
        visibilityFullFit:true,
        cssWidthAndHeight:true,
        slidesPerView:'auto',
        centeredSlides:true,
        loop:true,
        mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
      });
  },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{option.value}}"><p>{{option.name}}</p></div></div></div>';
    }
  }

})
.directive('cookingoptionsslider',function($timeout){
  function link(scope,element,attrs){
    scope.menuOptions = [
      // {value:'myrecipes',name: 'Favorites'},
      {value:'timed',name:'Cook by Time'},
      {value:'null', name:'Cook Twice'},
      {value:'null', name:'by Food Type'},
      {value:'null', name:'Defrost'},
      {value:'null', name: 'Beverage'},
      {value:'null', name:'Popcorn'},
      {value:'null', name:'Melt'},
      {value:'null', name:'Reheat'},
      {value:'null', name:'Simmer'},
      {value:'null', name:'Soften'}
    ];
   $timeout(function() {
      currentSlider = $('.swiper-container').swiper({ 
        speed:100, 
        slideshow:false,
        controlNav: false,
        directionNav: false,
        // initialSlide:1,
        slidesPerViewFit:true,
        visibilityFullFit:true,
        cssWidthAndHeight:true,
        slidesPerView:'auto',
        centeredSlides:true,
        loop:false,
        mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
      });
  },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{option.value}}"><p>{{option.name}}</p></div></div></div>';
    }
  }
})
.directive('cookingmodenexus',function($timeout){
  function link(scope,element,attrs){
    scope.menuOptions = [
      // {value:'myrecipes',name: 'Favorites'},
      {value:'speedcook',name:'Speed cook'},
      {value:'null', name:'Oven'},
      {value:'null', name:'Microwave'}
    ];
      $timeout(function() {
        $('.ovenoptions a.option').each(function(){

            $(this).click(function(e) {
              // var x = e.container;
              var y = $(this).data('val');
              y = (y !==undefined)?y:prevVal;
              scope.currentCall = prevVal = y;
              if(y =='speedcook'){

                $('.ovenpanel .settings, .ovenoptions, .ovenpanel .remotetimer, .ovenpanel .remote').animate({opacity:0},300);
                $('.ovenpanel').prepend('<div class="currentmode">Speed Cook</div>');
                $('.ovenpanel .currentmode').css('top','146px').css('opacity','0').animate({opacity:1},300,function(){
                  $('.ovenpanel .currentmode').animate({top:'16px'},700,function(){
                    scope.applianceEnterPressed(y);
                  });
                });
           
              } else{
                scope.applianceEnterPressed(y);
              }
          });
        });
      },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions"><a class="option {{option.value}}" ng-repeat="option in menuOptions" data-val="{{option.value}}">{{option.name}}</a></div>';
    }
  }
})
.directive('cookingmodeslider',function($timeout){
  function link(scope,element,attrs){
    scope.menuOptions = [
      {value:'myrecipes',name: 'Favorites'},
      {value:'speedcook',name:'Speedcook'},
      {value:'null', name:'Oven'},
      {value:'microwave', name:'Microwave'}
    ];
   $timeout(function() {
      currentSlider = $('.swiper-container').swiper({ 
        speed:100, 
        slideshow:false,
        controlNav: false,
        directionNav: false,
        initialSlide:1,
        slidesPerViewFit:true,
        visibilityFullFit:true,
        cssWidthAndHeight:true,
        slidesPerView:'auto',
        centeredSlides:true,
        loop:false,
        mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
      });
  },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{option.value}}"><p>{{option.name}}</p></div></div></div>';
    }
  }
})
.directive('stepsnavigation',function($timeout){
  function link(scope,element,attrs){
    // scope.initFirebase(function() {
      scope.menuOptions = scope.comm.presets.speedcook;
      $timeout(function() {/* start $timeout */

      },5);/* end $timeout */
      scope.goBack = function(){
        // console.log('scope.previousCall',scope.previousCall);
        if(scope.comm.timer_running===false){
          if(scope.previousCall==''){
            scope.currentlcd = scope.homelcd;
            scope.currentCall = 'cookingmodes';
            hndlr.hidePrompt();
          } else {
            scope.currentCall = scope.previousCall;
            scope.applianceEnterPressed();
          }
        } else {
          audioHandler.playEnter();
        }
      };

    // });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="stepsnav"><a class="prev" ng-click="goBack()"></a><a class="next" ng-click="applianceEnterPressed()"></a></div>';
    }
  }
})
.directive('foodtypeslidernexus',function($timeout){
  function link(scope,element,attrs){
    // scope.initFirebase(function() {
      scope.menuOptions = scope.comm.presets.speedcook;

      $timeout(function() {
        /* start $timeout */

        // $('.swiper-slide[data-val="myrecipes"]').detach();
        var y = $('.swiper-slide[data-val="myrecipes"]').detach();
        $('.ovenoptions .swiper-wrapper').append(y);

        currentSlider = $('.swiper-container').swiper({ 
          speed:300,
          // height:160, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
          // watchSlidesProgress:true,
          // watchSlidesVisibility:true,
          // slidesPerViewFit:false,
          // visibilityFullFit:false,
          // controlBy:'container',
          // cssWidthAndHeight:true,
          slidesPerView:6,
          centeredSlides:false,
          longSwipes:true,
          longSwipesRatio:2,
          // shortSwipesRatio:false,
          grabCursor: true,
          mode:'vertical',
          onTouchMove:function(swiper, event){
            $('.swiper-slide').not('.swiper-slide-duplicate').each(function(){
              // console.log($(this),$(this).offset().top);
              if(($(this).offset().top > 157)&&($(this).offset().top < 185)){
                // console.log('found');
                var newVal = 30-Math.abs(($(this).offset().top-157))*.1875; 
                $(this).find('p').css('font-size',newVal+'px');
              } else{
                $(this).find('p').css('font-size','');
              }

            })
          },
            onTouchEnd:function(e){
              scope.updateCall(e);
            },
            onSwiperCreated:function(e) {
              scope.updateCall(e);
            },
            onInit:function(e) {
              scope.updateCall(e);
            },
            onSlideChangeEnd:function(e) {
              scope.updateCall(e);
            }
          });

      },10);
    /* end $timeout */
    // });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" data-val="appetizers"><p>appetizers</p></div><div class="swiper-slide" data-val="breads"><p>breads</p></div><div class="swiper-slide" data-val="breakfast"><p>breakfast</p></div><div class="swiper-slide" data-val="chicken"><p>chicken</p></div><div class="swiper-slide" data-val="desserts"><p>desserts</p></div><div class="swiper-slide" data-val="entree"><p>entree</p></div><div class="swiper-slide" data-val="meats"><p>meats</p></div><div class="swiper-slide" data-val="myrecipes"><p>My Recipes</p></div><div class="swiper-slide" data-val="pizza"><p>pizza</p></div><div class="swiper-slide" data-val="potatoes"><p>potatoes</p></div><div class="swiper-slide" data-val="sandwich"><p>sandwich</p></div><div class="swiper-slide" data-val="seafood"><p>seafood</p></div><div class="swiper-slide" data-val="side dish"><p>side dish</p></div></div></div>';
    }
  }
})
.directive('foodtypeslider',function($timeout){
  function link(scope,element,attrs){
    scope.initFirebase(function() {
      scope.menuOptions = scope.comm.presets.speedcook;
      $timeout(function() {
        var y = $('.swiper-slide[data-val="myrecipes"]').detach();
        // $('.ovenoptions .swiper-wrapper').prepend(y);
        currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
        slidesPerViewFit:true,
        visibilityFullFit:true,
        cssWidthAndHeight:true,
        slidesPerView:'auto',
        centeredSlides:true,
        loop:false,
        mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onInit:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
        });
      },5);
    });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{option.slug}}"><p>{{option.name}}</p></div></div></div>';
    }
  }
})
.directive('foodselectslidernexus',function($timeout){
  function link(scope,element,attrs){
    // scope.initFirebase(function() {
        scope.updateStatus('<div class="foodtype step">Chicken</div>');

      scope.menuOptions = scope.comm.presets.speedcook[scope.cur_foodtype].optionlist;
      $timeout(function() {
        currentSlider.destroy();
        currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:6,
          centeredSlides:false,
          longSwipes:true,
          longSwipesRatio:2,
          shortSwipesRatio:false,
          loop:false,
          mode:'vertical',
          onTouchMove:function(swiper, event){
            $('.swiper-slide').not('.swiper-slide-duplicate').each(function(){
              // console.log($(this),$(this).offset().top);
              if(($(this).offset().top > 157)&&($(this).offset().top < 185)){
                // console.log('found');
                var newVal = 30-Math.abs(($(this).offset().top-157))*.1875; 
                $(this).find('p').css('font-size',newVal+'px');
              } else{
                $(this).find('p').css('font-size','');
              }

            })
          },
          onTouchEnd:function(e){
            scope.updateCall(e);
          },
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onInit:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
        });
      },5);
    // });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" data-val="bonein"><p>Bone-In</p></div><div class="swiper-slide" data-val="boneless"><p>Boneless</p></div><div class="swiper-slide" data-val="frozenfillets"><p>Fillet (frozen)</p></div><div class="swiper-slide" data-val="frozenfinger"><p>Finger (frozen)</p></div><div class="swiper-slide" data-val="frozenfried"><p>Fried (frozen)</p></div><div class="swiper-slide" data-val="frozennugget"><p>Nugget (frozen)</p></div><div class="swiper-slide" data-val="frozenpatty"><p>Patty (frozen)</p></div><div class="swiper-slide" data-val="frozentender"><p>Tender (frozen)</p></div><div class="swiper-slide" data-val="frozenwings"><p>Wings (frozen)</p></div><div class="swiper-slide" data-val="whole"><p>Whole</p></div></div></div>';
    }
  }
})
.directive('foodselectslider',function($timeout){
  function link(scope,element,attrs){
    scope.initFirebase(function() {

      scope.menuOptions = scope.comm.presets.speedcook[scope.cur_foodtype].optionlist;
      $timeout(function() {
        currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:'auto',
          centeredSlides:true,
          loop:false,
          mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onInit:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
        });
      },5);
    });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{$index}}"><p>{{option.name}}</p></div></div></div>';
    }
  }
})
.directive('foodsizeslidernexus',function($timeout){
  function link(scope,element,attrs){
    // scope.initFirebase(function() {
      // scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;
      // scope.updateStatus('<div class="foodsize step">Small</div>');
      scope.updateStatus('<div class="foodsub step">Boneless</div>');

      var x = scope.comm.presets.speedcook[scope.cur_foodtype].optionlist;
      var y = scope.comm.presets.speedcook[scope.cur_foodtype].name;
      var cookdetailname = x[scope.cur_foodselectindex].name + ' ' + y;
      scope.comm.speedcook_title = cookdetailname;

      scope.menuOptions = scope.comm.presets.speedcook[scope.cur_foodtype].size;
  
      scope.cur_size = scope.menuOptions[0].label;
      scope.cur_sizedetail = scope.menuOptions[0].detail;
      scope.cur_sizevalue = scope.menuOptions[0].value;

      $timeout(function() {
        currentSlider.destroy();
        currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:6,
          centeredSlides:false,
          longSwipes:true,
          longSwipesRatio:2,
          shortSwipesRatio:false,
          loop:false,
          mode:'vertical',
          onTouchMove:function(swiper, event){
            $('.swiper-slide').not('.swiper-slide-duplicate').each(function(){
              // console.log($(this),$(this).offset().top);
              if(($(this).offset().top > 157)&&($(this).offset().top < 185)){
                // console.log('found');
                var newVal = 30-Math.abs(($(this).offset().top-157))*.1875; 
                $(this).find('p').css('font-size',newVal+'px');
              } else{
                $(this).find('p').css('font-size','');
              }

            })
          },
          onTouchEnd:function(e){
            scope.updateCall(e);
          },
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onInit:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
        });
      },5);
    // });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" data-val="small"><p>small <span>(4-6 oz ea)</span></p></div><div class="swiper-slide" data-val="medium"><p>medium <span>(7-9 oz ea)</span></p></div><div class="swiper-slide" data-val="large"><p>large <span>(10-12 oz ea)</span></p></div></div></div>';
    }
  }
})
.directive('foodsizeslider',function($timeout){
  function link(scope,element,attrs){
    scope.initFirebase(function() {
      // scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;

      var x = scope.comm.presets.speedcook[scope.cur_foodtype].optionlist;
      var y = scope.comm.presets.speedcook[scope.cur_foodtype].name;
      var cookdetailname = x[scope.cur_foodselectindex].name + ' ' + y;
      scope.comm.speedcook_title = cookdetailname;

      scope.menuOptions = scope.comm.presets.speedcook[scope.cur_foodtype].size;
  
      scope.cur_size = scope.menuOptions[0].label;
      scope.cur_sizedetail = scope.menuOptions[0].detail;
      scope.cur_sizevalue = scope.menuOptions[0].value;

      $timeout(function() {
        currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:'auto',
          centeredSlides:true,
          loop:false,
          mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onInit:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
        });
      },5);
    });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{option.label}}"><p>{{option.label}} <span>({{option.detail}})</span></p></div></div></div>';
    }
  }
})
.directive('foodquantslidernexus',function($timeout){
  function link(scope,element,attrs){
    // scope.initFirebase(function() {
      // scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;
      scope.updateStatus('<div class="foodsize step">Small</div>');

      scope.menuOptions = scope.comm.presets.speedcook[scope.cur_foodtype].quantity;
      scope.cur_quantity = scope.menuOptions[0].label;
      scope.cur_quantityvalue = scope.menuOptions[0].value;

      $timeout(function() {
        // currentSlider.destroy('onSwiperCreated');
        currentSlider.destroy();

        currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:6,
          centeredSlides:false,
          longSwipes:true,
          longSwipesRatio:2,
          shortSwipesRatio:false,
          loop:false,
          mode:'vertical',
          onTouchMove:function(swiper, event){
            $('.swiper-slide').not('.swiper-slide-duplicate').each(function(){
              // console.log($(this),$(this).offset().top);
              if(($(this).offset().top > 157)&&($(this).offset().top < 185)){
                // console.log('found');
                var newVal = 30-Math.abs(($(this).offset().top-157))*.1875; 
                $(this).find('p').css('font-size',newVal+'px');
              } else{
                $(this).find('p').css('font-size','');
              }

            })
          },
          onTouchEnd:function(e){
            scope.updateCall(e);
          },
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onInit:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
        });
      },5);
    // });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" data-val="1-2 pieces"><p>1-2 pieces</p></div><div class="swiper-slide" data-val="3-4 pieces"><p>3-4 pieces</p></div></div></div>';
    }
  }
})
.directive('foodquantslider',function($timeout){
  function link(scope,element,attrs){
    scope.initFirebase(function() {
      // scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;

      scope.menuOptions = scope.comm.presets.speedcook[scope.cur_foodtype].quantity;
  

      scope.cur_quantity = scope.menuOptions[0].label;
      scope.cur_quantityvalue = scope.menuOptions[0].value;

      $timeout(function() {
        // currentSlider.destroy('onSwiperCreated');
        currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:0,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:'auto',
          centeredSlides:true,
          loop:false,
          mode:'vertical',
          onSwiperCreated:function(e) {
            // console.log('onSwiperCreated');
            
            scope.updateCall(e);
            // scope.currentCall = '1-2 pieces';

          },
          onInit:function(e) {
            // console.log('onInit');
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            // console.log('onSlideChangeEnd');
            scope.updateCall(e);
          }
        });
      },5);
    });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{option.label}}"><p>{{option.label}}</p></div></div></div>';
    }
  }
})
.directive('roundtimer',function($timeout){
  function link(scope,element,attrs){
    // scope.initFirebase(function() {
      // scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;

      scope.menuOptions = scope.comm.presets.speedcook[scope.cur_foodtype].quantity;
  

      scope.cur_quantity = scope.menuOptions[0].label;
      scope.cur_quantityvalue = scope.menuOptions[0].value;
      
      $timeout(function() {
        function cleanupTL(){
          tl.clear();
        }
        // console.log('timer',scope.remainingTime,scope.comm.timer_val);
        var tl = new TimelineMax({repeat:0, yoyo:false, onComplete:cleanupTL});
        var tracer = $('.roundtimer .tracer');
        var mainFill = $('#rtimer .timefill');
        tracer.removeClass('on');

        tl.to(mainFill, 0, {drawSVG:false, });
        if($('.ovencooking').length>0){
          tl.to(mainFill, 27, {drawSVG:true,ease: Linear.ease});
          tracer.addClass('on');
          $('.roundtimer').addClass('on');
        }
        // $('.radial-progress').click(window.randomize);


      },0);
    // });

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="roundtimer"><svg version="1.1" baseProfile="tiny" id="rtimer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="250px" height="250px" viewBox="0 0 250 250" xml:space="preserve"><circle class="timerbg" fill="none" cx="125" cy="125" r="120" /><circle class="timefill" stroke-linecap="round" fill="none" cx="125" cy="125" r="120" transform="rotate(-90 125 125)" /></svg><div class="tracer"><img src="/img/svgs/tracer2.svg" width="260" height="260" /></div></div>';

      // return '<div class="radial-progress"><div class="circle"><div class="mask full"><div class="fill"></div></div><div class="mask half"><div class="fill"></div><div class="fill fix"></div></div><div class="shadow"></div></div><div class="inset"></div></div>';
    }
  }
})
.directive('settimeslider',function($timeout){
  function link(scope,element,attrs){
      scope.currentMin = 0;
      scope.currentSec = 0;
      // scope.quantities = $scope.comm.presets.speedcook[$scope.fdID].quantity;
      scope.minutesSet = false;
      scope.minutesArray = [];
      // scope.secondsArray = [{name:'00',value:0},{name:'15',value:15},{name:'30',value:30},{name:'45',value:45}];
      // for (var x = 0; x<100; x++){
      //   scope.minutesArray.push({name:((x < 10)?('0'+x):x),value:x});
      // };

      scope.secondsArray = [{name:'45',value:45},{name:'30',value:30},{name:'15',value:15},{name:'00',value:0}];
      for (var x = 99; x>=0; x--){
        scope.minutesArray.push({name:((x < 10)?('0'+x):x),value:x});
      };




      $timeout(function() {
       currentSlider = minSlider = $('.minutes.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          slidesPerView:5,
          initialSlide:99,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:'auto',
          centeredSlides:true,
          loop:false,
          mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCallandTimer(e);
          },
          onInit:function(e) {
            scope.updateCallandTimer(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCallandTimer(e);
          }
        });

        newSlider = $('.seconds.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          initialSlide:3,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:'auto',
          centeredSlides:true,
          loop:false,
          mode:'vertical',
          onSlideChangeEnd:function(e) {
            scope.updateCallandTimer(e);
          }
        });


      },5);

  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    templateUrl:'view/settimeslider_template.html'
  }
})
.directive('ovenpowerslider',function($timeout){
  function link(scope,element,attrs){
    scope.powerArray = [];
    for (var x = 10; x>0; x--){
      scope.powerArray.push({name:((x < 10)?('0'+x):x),value:x});
    };
    // scope.powerArray.push({name:'-',value:0});
   $timeout(function() {
      currentSlider = $('.swiper-container').swiper({ 
          speed:100, 
          slideshow:false,
          controlNav: false,
          directionNav: false,
          slidesPerViewFit:true,
          visibilityFullFit:true,
          cssWidthAndHeight:true,
          slidesPerView:'auto',
          centeredSlides:true,
          loop:false,
          mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCallandTimer(e);
          },
          onInit:function(e) {
            scope.updateCallandTimer(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCallandTimer(e);
          }
        });
  },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="ovenoptions swiper-wrapper"><div class="swiper-slide" ng-repeat="option in powerArray" data-power="{{option.value}}" data-val="timedpower"><p>{{option.name}}</p></div></div></div>';
    }
  }

})
.directive('oventimedmenuslider',function($timeout){
  function link(scope,element,attrs){
    scope.menuOptions = [
      {value:'starttimed',name:'start'},
      {value:'save',name:'save'}
    ];
   $timeout(function() {
      currentSlider = $('.swiper-container').swiper({ 
        speed:100, 
        slideshow:false,
        controlNav: false,
        directionNav: false,
        initialSlide:0,
        slidesPerViewFit:true,
        visibilityFullFit:true,
        cssWidthAndHeight:true,
        slidesPerView:'auto',
        centeredSlides:true,
        loop:false,
        mode:'vertical',
          onSwiperCreated:function(e) {
            scope.updateCall(e);
          },
          onInit:function(e) {
            scope.updateCall(e);
          },
          onSlideChangeEnd:function(e) {
            scope.updateCall(e);
          }
      });
  },5);
  }
  return {
    restrict:'E',
    transclude:true,
    link: link,
    controller: function(){},
    replace: true,
    template: function(elem, attr){
      return '<div class="ovenoptions swiper-container"><div class="swiper-wrapper"><div class="swiper-slide" ng-repeat="option in menuOptions" data-val="{{option.value}}"><p>{{option.name}}</p></div></div></div>';
    }
  }

})
.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $routeProvider.when('/',
  {
    templateUrl: baseURL+'view/home.html',
    controller: 'homeCtrl'
  })
  .when('/app/',
  {
    templateUrl: baseURL+'view/apphome.html',
    controller: 'appCtrl'
  })
  .when('/app/cancel/',
  {
    templateUrl: baseURL+'view/apphome.html',
    controller: 'stopCooking'
  })
  .when('/appnorc/',
  {
    templateUrl: baseURL+'view/apphome.html',
    controller: 'appCtrl2'
  })
  .when('/app/cooking/',
  {
    templateUrl: baseURL+'view/apphome.html',
    controller: 'appCtrlCooking'
  })
  .when('/app/speedcook/',
  {
    templateUrl: baseURL+'view/app_sc_1.html',
    controller: 'startSpeedCook'
  })
  .when ('/app/speedcook/:fdID',{
    templateUrl: baseURL+'view/app_sc_2.html',
      controller:'showFoodOptions'
  })
  .when ('/app/speedcook/:fdID/:optID',{
    templateUrl: baseURL+'view/app_sc_3.html',
      controller:'showOptions'
  })
  .when('/app/remotecook/',
  {
    templateUrl: baseURL+'view/cookingmode.html',
    controller: 'remotecook'
  })
  .when('/app/myrecipes/',
  {
    templateUrl: baseURL+'view/myrecipes_menu.html',
    controller: 'showMRmenu'
    // controller: 'myrecipesstart'
  })
  .when('/app/myrecipes/cookingmode/',
  {
    templateUrl: baseURL+'view/myrecipes_menu1.html',
    controller: 'showMRMode'
    // controller: 'myrecipesstart'
  })
   .when('/app/myrecipes/speedcook/',
  {
    templateUrl: baseURL+'view/myrecipes_menu2.html',
    controller: 'showMRmenu'
  })
  .when('/app/myrecipes/speedcook/foodtype/',
  {
    templateUrl: baseURL+'view/app_mr_sc_1.html',
    controller: 'showMyRecipesFoodType'
  })
  .when('/app/myrecipes/speedcook/:fdID',
  {
    templateUrl: baseURL+'view/app_mr_sc_2.html',
    controller: 'showMRFoodOptions'
  })
  .when('/app/myrecipes/speedcook/:fdID/:optID',
  {
    templateUrl: baseURL+'view/app_mr_sc_3.html',
    controller: 'showMROptions'
  })
  .when ('/appliance/',{
    templateUrl: baseURL+'view/appliance.html',
    controller:'applianceCtrl'
  })
  .when ('/panel/',{
    templateUrl: baseURL+'view/appliance_panel.html',
    controller:'applianceCtrl'
  })
  .when ('/panel2/',{
    templateUrl: baseURL+'view/appliance_panel_nexus.html',
    controller:'applianceCtrl'
  })
  .when ('/appliance/speedcook/',{
    templateUrl: baseURL+'view/appliance.html',
    controller:'applianceCtrl'
  })
  .otherwise ({
    templateUrl: baseURL+'view/home.html',
    controller: 'homeCtrl'
  });
  $locationProvider.html5Mode(true);
  // $locationProvider.hashPrefix('#');
}])
.run(function($rootScope, $route, $location){
   $rootScope.$on('$locationChangeSuccess', function() {
        $rootScope.actualLocation = $location.path();
    });        

   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
          $('.view-animate-container').removeClass('back');
        if($rootScope.actualLocation === newLocation) {
             $('.view-animate-container').addClass('back');
        } else {
          // $('.view-animate-container').removeClass('back');
        }
    });
});



$(function() {
  // menu
  $("#menu-button, #menu-close").click(function() {
    $("#main-nav").toggleClass("on");
  });
  audioHandler.init();
  // $('body').flowtype({
  //    // minFont : 15,
  //    maximum : 600
  // });
  $('.hover').on('touchstart touchend', function(e) {
      e.preventDefault();
      $(this).toggleClass('hover_effect');
  });

  //PREVENT FIRST SLIDE ANIMATION
 setTimeout(function() {
   $('.view-animate-container').removeClass('first-load');
 },1100);

});
