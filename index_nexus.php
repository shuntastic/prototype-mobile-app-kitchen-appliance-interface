<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org" id="ng-app" ng-app="psgeprototype" ng-controller="mainCon">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=8;FF=3;OtherUA=4" />
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <title ng-bind="Page.title()">PushStart | Prototype</title>
        <base href="/" target="_self">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <link rel="apple-touch-icon" href="touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/main.css?v=14">
        <script src="js/vendor/modernizr-2.6.2.min.js?v=14"></script>
    </head>
    <body ng-class="mobileCon">
         <h1 class="fbstatus"></h1>
         <div class="filler"></div>
         <div class="view-animate-container first-load">
            <div class="ng-view view-animate"></div>
            <colorpick></colorpick>
            <div id="appdisable" class="disabledprompt"></div>
            <div class="remotedisabledprompt">
                <div class="prompt">
                    <div class="header"></div>
                    <div><a href="{{baseURL}}#/app/">OK</a></div>
                </div>
            </div>

        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="js/vendor/angular.min.js"></script>
        <script src="js/vendor/angular-route.min.js"></script>
        <script src="js/vendor/angular-animate.min.js"></script>
        <script src="js/vendor/angular-resource.min.js"></script>
        <script src="https://cdn.firebase.com/js/client/1.0.21/firebase.js"></script>
        <script src="https://cdn.firebase.com/libs/angularfire/0.8.2/angularfire.min.js"></script>
        <script src="js/vendor/idangerous.swiper.min.js"></script>

        <script src="js/plugins.min.js"></script>
        <script src="js/main.min.js?v=14"></script>
    </body>
</html>
